#!/bin/bash
#echo  "fgrep docker /etc/group || addgroup docker; groupmod -g $(ls -gn /var/run/docker.sock|awk '{print $3;}') docker; usermod -aG docker jenkins" | sudo -s

fgrep docker /etc/group || addgroup docker; groupmod -g $(ls -gn /var/run/docker.sock|awk '{print $3;}') docker; usermod -aG docker jenkins 


while [ ! -f /usr/share/jenkins/rancher/plugins.txt ]; do
    sleep 1
done

/usr/local/bin/plugins.sh /usr/share/jenkins/rancher/plugins.txt
exec /bin/tini -- /usr/local/bin/jenkins.sh
