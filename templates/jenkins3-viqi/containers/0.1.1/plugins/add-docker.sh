#!/bin/bash
#echo  "fgrep docker /etc/group || addgroup docker; groupmod -g $(ls -gn /var/run/docker.sock|awk '{print $3;}') docker; usermod -aG docker jenkins" | sudo -s

echo "Installing docker client"

wget -q  https://get.docker.com/builds/Linux/x86_64/docker-17.03.0-ce.tgz
tar xzf docker-17.03.0-ce.tgz docker/docker
mv docker/docker /usr/bin/docker
rm -rf docker-17.03.0-ce.tgz docker/


fgrep docker /etc/group || addgroup docker; groupmod -g $(ls -gn /var/run/docker.sock|awk '{print $3;}') docker; usermod -aG docker jenkins


/usr/share/jenkins/rancher/su-exec.amd64 jenkins /usr/share/jenkins/rancher/jenkins.sh
